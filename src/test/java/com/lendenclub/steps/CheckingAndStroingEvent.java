package com.lendenclub.steps;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

public class CheckingAndStroingEvent {

	WebDriver driver;
	List<WebElement> countrow;
	List<WebElement> countrow1;
	String event[] = new String[100], columName[] = { "Event Name", "Event ID  ", "Event date & Time " };
	String version[] = new String[20];
//	String metabase[] = new String[100];
	String Row[] = { " ", " ", " " };
	
	String columName2[] = { "Checklist" , "Registration Fee  ", "Modified Date "};
	String version1[] = new String[20];
	String metadata[] = new String[100];
	String Row1[] = { " ", " ", " " };
	
	CSVWriter writer;

	public CheckingAndStroingEvent(WebDriver driver)
			throws IOException, FileNotFoundException, CsvValidationException, InterruptedException {
		this.driver = driver;

		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div/div/div/div[2]/ul/li[1]/a")).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.linkText("All Settings")).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Testing Console")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		String[] values;
		CSVReader reader = new CSVReader(new FileReader("./ReadExcel/test1.csv"));
		while ((values = reader.readNext()) != null) {
			for (int a = 0; a < values.length; a++) {
				System.out.print(values[a] + " ");

				System.out.println(" ");
				
				driver.findElement(By.id("inputId")).clear();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.id("inputId")).sendKeys(values);

				try {
					driver.findElement(By.xpath("//*[ contains (text(), 'View device data' ) ]")).click();
				} catch (Exception e) {
				}
				try {
					driver.findElement(By.xpath("//*[ contains (text(), 'Inspect device' ) ]")).click();
				} catch (Exception e) {
				}

				try {
					WebDriverWait wait = new WebDriverWait(driver, 3);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
				} catch (Exception ex) {
					ex.getMessage();
				}

				// for scroll to view all event id
				driver.manage().window().fullscreen();
				try {
					JavascriptExecutor js = (JavascriptExecutor) driver;
					WebElement Element = driver.findElement(By.xpath(
							"//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[2]/button"));
					js.executeScript("arguments[0].scrollIntoView();", Element);
				} catch (Exception e) {
				}

				
				writer = new CSVWriter(new FileWriter("./Resources/BorrowerStatus.csv", true));

				String error=null;
				try {
					error=(driver.findElement(By.xpath("//*[@id=\"angular-sidebar-portal\"]/div/div/div/p")).getText().toString());
				}catch(Exception e) {}
				System.out.println(error);

				if( error != null)
				{  	
					System.out.println("I am inside the errror");
					String failColumnName[]=new String[10];
					failColumnName[0]="Device-Id";
					failColumnName[1]="Status";
					failColumnName[2]="Reason";
					writer.writeNext(failColumnName);

					failColumnName[2]="Fail";
					failColumnName[1]=driver.findElement(By.className("react-modal__content__message")).getText();
					failColumnName[0]=values[a];
					writer.writeNext(failColumnName);
					driver.findElement(By.xpath("//*[@id=\"angular-sidebar-portal\"]/div[1]/div/div/div/button")).click();


				}
				else {

					countrow=(List<WebElement>) driver.findElements(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[10]/div/div"));
					int rowsize=countrow.size();

					try {
						JavascriptExecutor js = (JavascriptExecutor) driver;		
						WebElement Element = driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[2]/button"));
						js.executeScript("arguments[0].scrollIntoView();", Element);
					} catch(Exception e) {}

					version[0]="Device-Id";
					version[1]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[2]/div/div[2]")).getText().toString();

					writer.writeNext(version);

					int m=0;
					for(int n=1;n<=2; n++)
					{
						event[m]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[4]/div/div["+n+"]")).getText();
						version[m]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[7]/div/div["+n+"]")).getText();
						m++;
					}

					writer.writeNext(event);
					writer.writeNext(version);

					writer.writeNext(columName);
					for(int i=2; i<=rowsize;i++)
					{
						for(int j=1;j<=2;j++)
						{
							event[j]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[10]/div/div["+i+"]/div["+j+"]")).getText();
						}
						event[0]=validateEventID(event[1]);

						writer.writeNext(event);
					}
				}

//				MetabaseData();
				((JavascriptExecutor)driver).executeScript("window.open()");
				ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs.get(1));
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

				driver.get("https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjo0LCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjM3NSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MzkyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsMzY0MF0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsMzQzNl1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgVGFzayJ9XSwiZmlsdGVyIjpbImFuZCIsWyJjb250YWlucyIsWyJmaWVsZC1pZCIsMzYyM10sIjJlMDE5ODVlLWM3NzItNGVmMi1hYWQyLWRlYTczZTk2YmI3NSIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XV0sImJyZWFrb3V0IjpbWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMzYyOF0sImhvdXIiXSxbImJpbm5pbmctc3RyYXRlZ3kiLFsiZmllbGQtaWQiLDM2NDddLCJkZWZhdWx0Il0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsMzQ0MF1dXSwib3JkZXItYnkiOltbImRlc2MiLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDM2MjhdLCJob3VyIl1dXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5IjoidGFibGUiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=");
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				//		try {
				//			WebDriverWait wait = new WebDriverWait(driver, 1);
				//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
				//		} catch (Exception ex) {
				//			ex.getMessage();
				//		}
		try {
				driver.findElement(By.name("username")).click();
				driver.findElement(By.name("username")).clear();
				driver.findElement(By.name("username")).sendKeys("shubham.s@lendenclub.com");
				driver.findElement(By.name("password")).clear();
				driver.findElement(By.name("password")).sendKeys("shubham@123");
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.xpath("//*[text()='Sign in']")).click();
				//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch(Exception ex)  { }

				try {
					WebDriverWait wait = new WebDriverWait(driver, 1);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
				} catch (Exception ex) {
					ex.getMessage();
				}
				//		driver.navigate().refresh();
				((JavascriptExecutor)driver).executeScript("window.open()");
				ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs1.get(2));
				driver.get("https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjo0LCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjM3NSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MzkyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsMzY0MF0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsMzQzNl1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgVGFzayJ9XSwiZmlsdGVyIjpbImFuZCIsWyJjb250YWlucyIsWyJmaWVsZC1pZCIsMzYyM10sIjJlMDE5ODVlLWM3NzItNGVmMi1hYWQyLWRlYTczZTk2YmI3NSIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XV0sImJyZWFrb3V0IjpbWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMzYyOF0sImhvdXIiXSxbImJpbm5pbmctc3RyYXRlZ3kiLFsiZmllbGQtaWQiLDM2NDddLCJkZWZhdWx0Il0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsMzQ0MF1dXSwib3JkZXItYnkiOltbImRlc2MiLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDM2MjhdLCJob3VyIl1dXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5IjoidGFibGUiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=");


				try {
					WebDriverWait wait = new WebDriverWait(driver, 2);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
				} catch (Exception ex) {
					ex.getMessage();
				}

//				String[] value;
//				CSVReader reader = new CSVReader(new FileReader("./ReadExcel/test1.csv"));
//				while ((value = reader.readNext()) != null) {
//					for (int p = 0; p < value.length; p++) {
//						System.out.print(value[p] + " ");

//						System.out.println(" ");

						driver.manage().window().fullscreen();
						WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a/span[1]"));
						element.click();
						try {
							WebDriverWait wait = new WebDriverWait(driver, 1);
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
						} catch (Exception ex) {
							ex.getMessage();
						}

						WebElement element1 = driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul/li/input"));								
						element1.sendKeys(Keys.CONTROL+"A");
						element1.sendKeys(Keys.BACK_SPACE);
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

						driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul/li/input")).sendKeys(values[a]);
						driver.findElement(By.xpath("//*[ contains (text(), 'Update filter') ]")).click();
						driver.findElement(By.xpath("//*[ contains (text(), 'Update filter') ]")).click();	
//						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						try {
							WebDriverWait wait = new WebDriverWait(driver, 1);
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
						} catch (Exception ex) {
							ex.getMessage();
						}
							
							writer.writeNext(columName2);

							metadata[0] = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[3]/div")).getText();

							String data = (driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div"))).getText().toString();
							System.out.println("The data is"+ data);

							if( data != " " )
							{
								metadata[1] = data;
								//						event[1] = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div")).getText();
								//*[@id="root"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div
							}
							else {
								metadata[1] = "Data not Found";
							}

							metadata[2] = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/div")).getText();

							
							
							writer.writeNext(metadata);
							((JavascriptExecutor)driver).executeScript("window.open()");
							ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
							driver.switchTo().window(tabs2.get(0));
							
					}
			
			writer.writeNext(Row);
			writer.flush();
			
				}
		
		
				
				


			}

	private String validateEventID(String test) {

		String eventName = null;

		if (test.contains("gnvwv5")) {
			eventName = "SIGN_UP_TOKEN";
		} else if (test.contains("mdrxnp")) {
			eventName = "SOCIAL_LOGIN_EVENT_TOKEN ";
		} else if (test.contains("dypky0")) {
			eventName = "PERSONAL_DETAILS_EVENT_TOKEN  ";
		} else if (test.contains("d6vjuz")) {
			eventName = "ELIGIBLE_EVENT_TOKEN";
		} else if (test.contains("a4rb95")) {
			eventName = "LOAN_REQUIREMENT_EVENT_TOKEN ";
		} else if (test.contains("r9gg6y")) {
			eventName = "REGISTRATION_FEES_CONFIRMATION_TOKEN ";
		} else if (test.contains("n4fsxu")) {
			eventName = "LIVE_KYC_EVENT_TOKEN";
		} else if (test.contains("tz4fwx")) {
			eventName = "RESIDENTIAL_DETAILS_EVENT_TOKEN";
		} else if (test.contains("wr6olb")) {
			eventName = "CREDIT_DETAILS_EVENT_TOKEN  ";
		} else if (test.contains("d6fkc8")) {
			eventName = "BANK_STATEMENT_EVENT_TOKEN  ";
		} else if (test.contains("ka63bc")) {
			eventName = "BANK_DETAILS_EVENT_TOKEN ";
		} else if (test.contains("izm72m")) {
			eventName = "AUTO_DEBIT_EVENT_TOKEN ";
		} else if (test.contains("uw8uoe")) {
			eventName = "LOAN_AGREEMENT_EVENT_TOKEN ";
		} else if (test.contains("ka63bc")) {
			eventName = "BANK_DETAILS_EVENT_TOKEN ";
		} else if (test.contains("b8ebd9")) {
			eventName = "CANCELLED_LOAN_EVENT_TOKEN";
		} else if (test.contains("u65anr")) {
			eventName = "CLOSED_LOAN_EVENT_TOKEN";
		} else if (test.contains("sttovk")) {
			eventName = "DECLINED_LOAN_EVENT_TOKEN";
		} else if (test.contains("om5n31")) {
			eventName = "DISBURSED_LOAN_EVENT_TOKEN";
		} else if (test.contains("cs62tl")) {
			eventName = "LISTED_LOAN_EVENT_TOKEN";
		} else if (test.contains("1h1e4z")) {
			eventName = "OPEN_LOAN_EVENT_TOKEN";
		} else if (test.contains("pyekiv")) {
			eventName = "PROCESSING_LOAN_EVENT_TOKEN";
		} else {

			eventName = "To be taken from Amir shaikh";
		}

		return eventName;
		
	}
}
