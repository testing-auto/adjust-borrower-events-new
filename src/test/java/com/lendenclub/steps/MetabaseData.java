package com.lendenclub.steps;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

public class MetabaseData {
	WebDriver driver;
//	List<WebElement> countrow;
//	List<WebElement> countrow1;
	String metadata[] = new String[100];
//	String columName[] = { "Event Name", "Event ID  ", "Event date & Time " };
	String columnName1[] = {" ", " ", " "};
	String columName2[] = { "Checklist" , "Registration Fee  ", "Modified Date "};
	String[] version = new String[20];
//	String metabase[] = new String[100];
	String Row1[] = { " ", " ", " " };
	CSVWriter writer;
	
	public MetabaseData(WebDriver driver) throws CsvValidationException, IOException
	{
		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.get("https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjo0LCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjM3NSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MzkyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsMzY0MF0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsMzQzNl1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgVGFzayJ9XSwiZmlsdGVyIjpbImFuZCIsWyJjb250YWlucyIsWyJmaWVsZC1pZCIsMzYyM10sIjJlMDE5ODVlLWM3NzItNGVmMi1hYWQyLWRlYTczZTk2YmI3NSIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XV0sImJyZWFrb3V0IjpbWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMzYyOF0sImhvdXIiXSxbImJpbm5pbmctc3RyYXRlZ3kiLFsiZmllbGQtaWQiLDM2NDddLCJkZWZhdWx0Il0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsMzQ0MF1dXSwib3JkZXItYnkiOltbImRlc2MiLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDM2MjhdLCJob3VyIl1dXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5IjoidGFibGUiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//		try {
		//			WebDriverWait wait = new WebDriverWait(driver, 1);
		//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
		//		} catch (Exception ex) {
		//			ex.getMessage();
		//		}

		driver.findElement(By.name("username")).click();
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys("shubham.s@lendenclub.com");
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys("shubham@123");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[text()='Sign in']")).click();
		//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
		} catch (Exception ex) {
			ex.getMessage();
		}
		//		driver.navigate().refresh();
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));
		driver.get("https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjo0LCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjM3NSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MzkyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsMzY0MF0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsMzQzNl1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgVGFzayJ9XSwiZmlsdGVyIjpbImFuZCIsWyJjb250YWlucyIsWyJmaWVsZC1pZCIsMzYyM10sIjJlMDE5ODVlLWM3NzItNGVmMi1hYWQyLWRlYTczZTk2YmI3NSIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XV0sImJyZWFrb3V0IjpbWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMzYyOF0sImhvdXIiXSxbImJpbm5pbmctc3RyYXRlZ3kiLFsiZmllbGQtaWQiLDM2NDddLCJkZWZhdWx0Il0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsMzQ0MF1dXSwib3JkZXItYnkiOltbImRlc2MiLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDM2MjhdLCJob3VyIl1dXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5IjoidGFibGUiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=");


		try {
			WebDriverWait wait = new WebDriverWait(driver, 2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
		} catch (Exception ex) {
			ex.getMessage();
		}

		String[] value;
		CSVReader reader = new CSVReader(new FileReader("./ReadExcel/test1.csv"));
		while ((value = reader.readNext()) != null) {
			for (int p = 0; p < value.length; p++) {
				System.out.print(value[p] + " ");

				System.out.println(" ");

				driver.manage().window().fullscreen();
				WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a/span[1]"));
				element.click();
				try {
					WebDriverWait wait = new WebDriverWait(driver, 1);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
				} catch (Exception ex) {
					ex.getMessage();
				}

				WebElement element1 = driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul/li/input"));								
				element1.sendKeys(Keys.CONTROL+"A");
				element1.sendKeys(Keys.BACK_SPACE);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul/li/input")).sendKeys(value);
				driver.findElement(By.xpath("//*[ contains (text(), 'Update filter') ]")).click();
				driver.findElement(By.xpath("//*[ contains (text(), 'Update filter') ]")).click();	

				//				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				//				driver.findElement(By.xpath("//*[ contains (text(), 'Modified Date: Hour') ]")).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				writer = new CSVWriter(new FileWriter("./Resources/BorrowerStatus.csv", true));
//				FileWriter writer = new FileWriter("./Resources/BorrowerStatus.csv", true);

				
//				String expectedError = "No results!";
				
//				String error=(driver.findElement(By.xpath("//*[ contains (text(), 'No results!') ]")).getText().toString());
				//
				//				}catch(Exception e) {}
				//				System.out.println(error);

//				if(error != null)
//				{  	
//					System.out.println("I am inside the errror");
//					String failColumnName[]=new String[10];
//					failColumnName[0]="Device-Id";
//					failColumnName[1]="Status";
//					failColumnName[2]="Reason";
//					writer.writeNext(failColumnName);
//
//					//					failColumnName[0]=value[p];
//					failColumnName[0]="123456";
//					failColumnName[1]= "Hello";
//					failColumnName[2]="Fail";
//					//					driver.findElement(By.xpath("//*[ contains (text(), 'No results' ) ]")).getText();
//
//					writer.writeNext(failColumnName);
//				}
//
//				else {                                                         
					//					countrow=(List<WebElement>) driver.findElements(By.xpath(" //*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div"));
					//
					//					int rowlength=countrow.size();
					//					System.out.println("Total Row size is : "+rowlength);

					version[0]="Device-Id";
					version[1]=value[p];
					writer.writeNext(version);
					
//					writer.writeNext(columnName1);
//					writer.writeNext(columnName1);
					
					writer.writeNext(columName2);

					metadata[0] = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[3]/div")).getText();

					String data = (driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div"))).getText().toString();
					System.out.println("The data is"+ data);

					if( data != " " )
					{
						metadata[1] = data;
						//						event[1] = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div")).getText();
						//*[@id="root"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div
					}
					else {
						metadata[1] = "Data not Found";
					}

					metadata[2] = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/div")).getText();

					writer.writeNext(metadata);

					writer.writeNext(Row1);
					writer.flush();
			}
		}

	}

}
