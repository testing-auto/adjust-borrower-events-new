package com.lendenclub.selenium;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebCapability {
	
static WebDriver driver;
	
	public static WebDriver WebCapability()
	{
		try 
		{
			ChromeOptions opt = new ChromeOptions();
			WebDriverManager.chromedriver().setup();
			opt.addArguments("headless");
			driver = new ChromeDriver();
			driver.manage().window().fullscreen();
			
			File source = new File("./Resources");
			try {
			
				FileUtils.cleanDirectory(source);
			
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		
		}
		catch(Exception e)
		{}
		
		return driver;
	}

}
