package com.lendenclub.test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.lendenclub.selenium.WebCapability;
import com.lendenclub.steps.CheckingAndStroingEvent;
import com.lendenclub.steps.Login;
import com.lendenclub.steps.MetabaseData;
import com.opencsv.exceptions.CsvValidationException;

public class TestExecution extends WebCapability{
	
	WebDriver driver;
	
	@BeforeTest
	public void openingChrome()
	{	   
		driver = WebCapability();
	}
	
	@Test(priority = 1)
	public void  loginPage() throws Exception
	{
	  	new Login(driver);
	  	
	}
	
	@Test(priority = 2)
	public void  CheckingJobStatus() throws Exception
	{
	  	new CheckingAndStroingEvent(driver);
	  	
	}
	
//	@Test(priority = 3)
//	public void testMetabaseData() throws CsvValidationException, IOException {
//		new MetabaseData(driver);
//	}
	
	@AfterTest
	public void CloseBrowser()
	{
		
	 	driver.quit();
	}

}
